#ifndef __STUDIO_STATUS_WIDGET_H
#define __STUDIO_STATUS_WIDGET_H


#include <gtk/gtk.h>

#include "builtin-studio.h"

void studio_main_status_widget_new(struct studio_context *, GtkWidget *);


#endif /* __STUDIO_STATUS_WIDGET_H */
